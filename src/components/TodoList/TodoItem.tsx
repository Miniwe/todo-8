import React, { useRef } from 'react'
import { useDrag, useDrop, DropTargetMonitor } from 'react-dnd'
import { XYCoord } from 'dnd-core'
import {
  TodoItemWrapper,
  TextContainer,
  ListName,
  FavIcon,
} from './TodoItem.styled'
import { IconContainer } from '../IconContainer'
import { ITodo, IDrag, IDragItem } from '../../interfaces'

interface ITodoExtended extends ITodo, IDrag {
  onToggle?: (id: number) => void
}

const TodoItem: React.FC<ITodoExtended> = ({
  todo,
  id,
  completed,
  onToggle,
  onRemove,
  index,
  moveCard,
}) => {
  const ref = useRef<HTMLDivElement>(null)

  const handleToggle = () => {
    if (onToggle) {
      onToggle(id)
    }
  }
  const handleRemove = () => {
    if (onRemove) {
      onRemove(id)
    }
  }

  const [, drop] = useDrop({
    accept: 'ItemTypes.CARD',
    hover(item: IDragItem, monitor: DropTargetMonitor) {
      if (!ref.current) {
        return
      }
      const dragIndex = item.index
      const hoverIndex = index

      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return
      }

      // Determine rectangle on screen
      const hoverBoundingRect = ref.current?.getBoundingClientRect()

      // Get vertical middle
      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

      // Determine mouse position
      const clientOffset = monitor.getClientOffset()

      // Get pixels to the top
      const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top

      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%

      // Dragging downwards
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return
      }

      // Dragging upwards
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return
      }

      // Time to actually perform the action
      moveCard(dragIndex, hoverIndex)

      // Note: we're mutating the monitor item here!
      // Generally it's better to avoid mutations,
      // but it's good here for the sake of performance
      // to avoid expensive index searches.
      item.index = hoverIndex
    },
  })

  const [{ isDragging }, drag] = useDrag({
    item: { type: 'ItemTypes.CARD', id, index },
    collect: (monitor: any) => ({
      isDragging: monitor.isDragging(),
    }),
  })

  drag(drop(ref))

  return (
    <TodoItemWrapper title={id.toString()} ref={ref} style={{ opacity: isDragging ? 0 : 1 }}>
      <IconContainer
        active={!!completed}
        activeIcon={<i className="las la-check-circle"></i>}
        inactiveIcon={<i className="las la-circle"></i>}
        onClick={handleToggle}
      />
      <TextContainer completed={completed} onClick={handleToggle}>
        {todo}
        <ListName>
          Tasks
        </ListName>
      </TextContainer>
      <FavIcon className="las la-trash" onClick={handleRemove}></FavIcon>
    </TodoItemWrapper>

  )
}

export default TodoItem
