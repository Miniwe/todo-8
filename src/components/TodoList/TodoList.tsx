import React from 'react'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'

import { TodoListWrapper, TodoCompletedCaption } from './TodoList.styled'
import TodoItem from './TodoItem'
import { IconContainer } from '../IconContainer'
import { ITodo, IDrag } from '../../interfaces'


interface ITodoExtended extends ITodo, IDrag {}

interface ITodoListProps {
  todos: ITodoExtended[]
  onToggle?: (id: number) => void
  onRemove?: (id: number) => void
}

const TodoList: React.FC<ITodoListProps> = ({ todos, onToggle, onRemove }) => {
  const showCompleted = true

  const incompleted: ITodoExtended[] = []
  const completed: ITodoExtended[] = []

  todos.forEach((item) => {
    if (item.completed) {
      completed.push(item)
    } else {
      incompleted.push(item)
    }
  })

  return (
    <DndProvider backend={HTML5Backend}>
      <TodoListWrapper>
        {incompleted.map((todo) => (
          <TodoItem
            key={todo.id}
            {...todo}
            onToggle={onToggle?.bind(null, todo.id)}
            onRemove={onRemove?.bind(null, todo.id)}
          />
        ))}
        {completed.length > 0 ? (
          <>
            <TodoCompletedCaption>
              <IconContainer
                active={showCompleted}
                activeIcon={<i className="las la-angle-down"></i>}
                inactiveIcon={<i className="las la-angle-right"></i>}
              />
              Compelted
            </TodoCompletedCaption>
            {completed.map((todo) => (
              <TodoItem
                key={todo.id}
                {...todo}
                onToggle={onToggle?.bind(null, todo.id)}
                onRemove={onRemove?.bind(null, todo.id)}
              />
            ))}
          </>
        ) : null}
        {!todos.length && <p style={{ textAlign: 'center' }}>No todos yet!</p>}
      </TodoListWrapper>
    </DndProvider>
  )
}

export default TodoList
