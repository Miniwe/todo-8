import styled from 'styled-components'

export const TodoListWrapper = styled.div`
  padding: 0 1rem;
`
export const TodoCompletedCaption = styled.div`
  font-weight: bold;
  font-size: 1.4rem;
  display: flex;
  align-items: center;
  cursor: pointer;
  height: 4rem;

  i {
    margin-right: 1rem;
  }
`
