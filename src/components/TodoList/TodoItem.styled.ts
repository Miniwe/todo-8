import styled from 'styled-components'

export const TodoItemWrapper = styled.div`
  display: flex;
  flex-direction: revert;
  cursor: pointer;
  margin: 0 -2rem;
  padding: 0 2rem;
  background-color: rgba(255, 255, 255, 0);
  box-shadow: 0 0 0 0 #fff;
  transition: background-color 0.2s ease, box-shadow 2.2s ease;

  &:hover {
    background-color: #ebebff;
    box-shadow: 3px 3px 9px -6px #00000088;
  }
`

interface ITextWrapperProps {
  completed: boolean
}

export const TextContainer = styled.div<ITextWrapperProps>`
  flex-grow: 1;
  padding: 1rem;
  font-size: 1.2rem;
  ${({ completed }) => completed ? 'text-decoration: line-through;' : ''}
`

export const ListName = styled.div`
  color: #666;
  font-size: 0.9rem;
  margin-top: 0.4rem;
`

export const FavIcon = styled.i`
  margin: 1rem 0 1rem 1rem;
  font-size: 1.8rem;
  display: inline-block;
  cursor: pointer;
  transform: scale(0.9);
  transition: transform 0.2s ease;
  color: #990000;

  &:hover {
    transform: scale(1.1);
  }
`