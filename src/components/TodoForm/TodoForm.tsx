import React, { useRef, useState, useEffect } from 'react'
import isEmpty from 'lodash/isEmpty'
import { TodoFormWrapper, InputForm, AddBtn } from './TodoForm.styled'
import { IconContainer } from '../IconContainer'

interface TodoFormProps {
  onAdd(todo: string): void
}

const TodoForm: React.FC<TodoFormProps> = ({ onAdd }) => {
  const inputRef = useRef<HTMLInputElement>(null)
  const [todo, setTodo] = useState<string>('')
  const [active, setActive] = useState<boolean>(false)

  const handleFocus = () => {
    setActive(true)
  }
  const handleBlur = () => {
    if (isEmpty(todo)) {
      setActive(false)
    }
  }

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTodo(event.target.value)
  }

  const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      event.preventDefault()
      event.stopPropagation()
      onAdd(todo)
      setTodo('')
    }
  }

  const handleAddClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    if (!isEmpty(todo)) {
      onAdd(todo)
      setTodo('')
      setActive(false)
    }
  }

  const handleIconClick = (event: React.MouseEvent) => {
    if (isEmpty(todo)) {
      inputRef.current!.focus()
    } else {
      setTodo('')
      setActive(false)
    }
  }

  useEffect(() => {
    if (isEmpty(todo)) {
      inputRef.current!.blur()
    }
  }, [todo])
  // TODO: CLICK ON ICON CONTAINER WILL CALL ACTION
  //  if empty focus on input
  //  else clear

  // TODO: ON ENTER or BUTTON CLICK SUBMIT
  // TODO: ON ESC  Clear INput
  // TODO
  return (
    <TodoFormWrapper active={active}>
      <IconContainer
        active={active}
        onClick={handleIconClick}
        activeIcon={<i className="las la-circle"></i>}
        inactiveIcon={<i className="las la-plus"></i>}
      />
      <InputForm
        type="text"
        ref={inputRef}
        onChange={handleChange}
        onKeyPress={handleKeyPress}
        value={todo}
        onFocus={handleFocus}
        onBlur={handleBlur}
        placeholder="Add Todo "
      />
      {active && <AddBtn type="button" onClick={handleAddClick}>Add</AddBtn>}
    </TodoFormWrapper>
  )
}

export default TodoForm
