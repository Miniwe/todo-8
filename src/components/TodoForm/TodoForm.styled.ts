import styled from 'styled-components'

interface ITodoFormWrapperProps {
  active: Boolean
}

export const TodoFormWrapper = styled.form<ITodoFormWrapperProps>`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin: 1rem;
  border-bottom: 1px solid ${({ active }) => active ? '#666' : '#fbfbfb'};
`;

export const FormIcon = styled.span`
`;

export const InputForm = styled.input`
  flex-grow: 1;
  padding: 1rem;
  border: 0;
`;

export const AddBtn = styled.button`
  font-size: 0.9rem;
  text-transform: uppercase;
  font-weight: bold;
  background: none;
  border: none;
  outline: none;
`;

