import React, { useEffect, useState } from 'react'
import { AppWrapper } from './App.styled'
import { Header } from '../Header'
import { TodoForm } from '../TodoForm'
import { TodoList } from '../TodoList'
import { Footer } from '../Footer'
import { ITodo, IDrag } from '../../interfaces'


interface ITodoExtended extends ITodo, IDrag {}

const App: React.FC = () => {
  const [todos, setTodos] = useState<ITodoExtended[]>([])

  useEffect(() => {
    const cached = JSON.parse(localStorage.getItem('todos') || '[]') as ITodoExtended[]
    setTodos(cached)
  }, [])

  useEffect(() => {
    localStorage.setItem('todos', JSON.stringify(todos))
  }, [todos])

  const handleToggle = (id: number) => {
    setTodos(prev => prev.map((item: ITodoExtended) => {
      if (item.id === id) {
        return {...item, completed: !item.completed}
      }
      return item
    }))
  }

  const handleRemove = (id: number) => {
    setTodos(prev => prev.filter(item => item.id !== id))
  }

  const handleAdd = ( todo: string ) => {
    const newTodo: ITodoExtended = {
      todo,
      id: Date.now(),
      completed: false,
      index: Date.now(),
      moveCard: (dragIndex, hoverIndex) => {},
    }
    setTodos(prev => [newTodo, ...prev])
  }

  return (
    <AppWrapper>
      <Header />
      <TodoForm onAdd={handleAdd} />
      <TodoList todos={todos} onToggle={handleToggle} onRemove={handleRemove} />
      <Footer />
    </AppWrapper>
  )
}

export default App
