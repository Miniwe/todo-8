import styled from 'styled-components'

export const AppWrapper = styled.div`
  margin: 0 auto;
  max-width: 960px;
  min-height: 100vh;
  width: 50vw;
  padding: 1rem;
  background: #fff;
  border: 1px solid #fbfbfb;
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  width: 100%;
`
