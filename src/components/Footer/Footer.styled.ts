import styled from 'styled-components'

export const FooterWrapper = styled.div`
  padding: 1rem;
  margin: 1rem;
  border-top: 1px solid #bfbfbf;
  margin-top: auto;
`
