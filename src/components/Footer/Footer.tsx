import React from 'react'
import { FooterWrapper } from './Footer.styled'

const Footer: React.FC = () => {
  return (
    <FooterWrapper>
      <code>
        <a
          href="https://bitbucket.org/Miniwe/todo-8/src/master/"
          target="_blank"
          rel="noopener noreferrer"
        >
          <strong>Project Repository</strong>
        </a>

      </code>
    </FooterWrapper>)
}

export default Footer
