import styled from 'styled-components'

export const HeaderWrapper = styled.div`
  margin: 1rem;
  border-bottom: 1px solid #fbfbfb;
  font-size: 1.2rem;
  background: #2e7d32;
  background: linear-gradient(192deg,#0700b8 0%,#0786b7 100%);
  color: #fff;
  padding: 2rem;
  margin: -1rem;
`
export const TitleBar = styled.div`
  display: flex;
  align-items: center;
`

export const Title = styled.div`
  font-size: 1.6rem;
  font-weight: bold;
`
export const TitleDots = styled.div`
  font-size: 1.6rem;
  font-weight: bold;
  margin-top: -0.4rem;
  margin-left: 1rem;
  color: #fff;
  opacity: 0.6;
`

export const TodayHint = styled.div`
  margin-left: auto;
  font-size: 1.2rem;

  i {
    font-size: 1.8rem;
    vertical-align: text-top;
  }
`

export const SortBtn = styled.div`
  margin-left: 2rem;
  i {
    font-size: 1.8rem;
    vertical-align: text-top;
    transform: rotate(90deg);
  }
`

export const Calendar = styled.div`
  font-size: 0.9rem;
  margin-top: 0.7rem;
`
