import React from 'react'
import format from 'date-fns/format'
import {
  HeaderWrapper,
  TitleBar,
  Title,
  TitleDots,
  TodayHint,
  SortBtn,
  Calendar,
} from './Header.styled'

const Header: React.FC = () => {
  return (
    <HeaderWrapper>
      <TitleBar>
        <Title>My Tasks</Title>
        <TitleDots>...</TitleDots>
        <TodayHint>
          <i className="las la-lightbulb"></i>
          Today
        </TodayHint>
        <SortBtn>
          <i className="las la-exchange-alt"></i>
        </SortBtn>
      </TitleBar>
      <Calendar>{format(new Date(), 'eeee, MMMM dd')}</Calendar>
    </HeaderWrapper>
  )
}

export default Header
