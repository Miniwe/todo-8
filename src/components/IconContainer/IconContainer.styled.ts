import styled from 'styled-components'

export const IconContainerWrapper = styled.div`
  margin: 1rem 0;
  cursor: pointer;
  transform: scale(0.9);
  transition: transform 0.2s ease;

  i {
    font-size: 1.8rem;
    display: inline-block;
  }

  &:hover {
    transform: scale(1.1);
  }
`
