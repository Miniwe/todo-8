import React from 'react'
import { IconContainerWrapper } from './IconContainer.styled'

interface IState {
  active: Boolean
  activeIcon: React.ReactNode
  inactiveIcon: React.ReactNode
  onClick?: (event: React.MouseEvent<Element, MouseEvent>) => void
}

const IconContainer: React.FC<IState> = ({
  active,
  activeIcon,
  inactiveIcon,
  onClick
}: IState) => {
  return (
    <IconContainerWrapper onClick={onClick}>
      {active ? activeIcon : inactiveIcon}
    </IconContainerWrapper>
  )
}

export default IconContainer
