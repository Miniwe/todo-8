export interface IDrag {
  index: number
  moveCard: (dragIndex: number, hoverIndex: number) => void
}

export interface IDragItem {
  index: number
  id: string
  type: string
}